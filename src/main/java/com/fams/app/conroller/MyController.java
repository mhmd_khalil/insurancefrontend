package com.fams.app.conroller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MyController {



    @GetMapping(path = "/message")
    @ResponseBody
    public String message() {
        System.out.println("Hello I'm here");
        return "hello";
    }
}
