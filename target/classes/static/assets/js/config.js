
window["production"] = false;
window['protocol'] = 'http:';
window['apiUrl'] = `${window["protocol"]}//15.185.42.1:7001/`;
window["loginApiUrl"] = `${window['apiUrl']}insurance/user/login`;
window['iserveApiUrl'] = `${window['apiUrl']}iserve`;
window['webSeocketEndPoint'] = `${window["apiUrl"]}insurance/cm-websocket`;
window['webSeocketTopic'] = `${window["apiUrl"]}topic/checkClaimStatus`;
window['SedRequestUrl'] = `${window["apiUrl"]}app/checkClaimStatus`;

